FROM alpine:3.10 as builder

RUN apk --no-cache update && apk --no-cache add make git gcc python3-dev openssl-dev libffi-dev musl-dev

WORKDIR /tempDir

COPY pyproject.toml .
COPY poetry.lock .
RUN pip3 install --upgrade pip
RUN pip3 install poetry==1.0.5
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-ansi


COPY . /tempDir
RUN git submodule init
RUN git submodule update

RUN make publish

FROM nginx:alpine
COPY --from=builder /tempDir/output/ /usr/share/nginx/html

