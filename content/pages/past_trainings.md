Title: Past Trainings 
Save_as: past_trainings.html
Template: page

# October 2019 Training

<hr/>

## Location

Cloud Business Center *(salle New York)*  
10 bis Rue du 4 septembre  
75002 Paris

## Schedule

Training sessions will start 9.30 am, a welcome coffee will be
available at 9 am.

<table class="table is-bordered">
  <thead>
    <tr>
      <th></th>
      <th>Wednesday</br>October 16th 2019</th>
      <th>Thursday</br>October 17th 2019</th>
      <th>Thursday</br>October 24th 2019</th>
      <th>Friday</br>October 25th 2019</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="2" style="vertical-align: middle"> Morning</th>
      <td>Tezos overview</td>
      <td rowspan="2" style="vertical-align: middle; text-align:center">Interact with the blockchain</td>
      <td rowspan="2" style="vertical-align: middle; text-align:center">LIGO</td>
      <td rowspan="5" style="vertical-align: middle; text-align:center">Hacking day</td>
    </tr>
    <tr>
      <td>Tezos standards</td>
     </tr>
    <tr>
      <th>Lunch</th>
      <td>-</td>
      <td>-</td>
      <td>-</td>
    </tr>
    <tr>
     <th rowspan="2" style="vertical-align: middle">Afternoon</th>
      <td>Self-amending blockchain</td>
      <td rowspan="2" style="vertical-align: middle; text-align:center"">Michelson Smart Contracts</td>
      <td rowspan="2" style="vertical-align: middle; text-align:center"">SmartPy</td>
    </tr>
    <tr>
      <td>Future of Tezos</td>
     </tr>
    <!-- <tr> -->
    <!--   <th>Evening</th> -->
    <!--   <td>Babylon Activation Party*</td> -->
    <!-- </tr> -->
   </tbody>
</table>


<!-- \***Babylon Activation Party** : 7PM  *La Revanche*,  38 Avenue Daumesnil, 75012 Paris -->


<hr/>

# Materials  

## Day 1:

- [Tezos overview][12]
- [Tezos standards][13]
- [Self-amending blockchain][14]
- [Futur of Tezos (privacy)][15]

## Day 2:

- [Interact with the blockchain][16]
- [Michelson smart contracts][17]

## Day 3:

- [Ligo][18]
- [Smartpy][19]

## Day 4:

**Example applications**

- [Call of a calculator contract in JavaScript.][20] Uses Taquito to communicate with a node.
- ["Chocolatine" vote contract][21] - Built with ReasonReact and uses Taquito.

# Prerequisites  

## Pre-configured Virtual machine (**recommended**)

To be sure to assist the sessions with a well configured environment, we
recommend to download the tailor-made virtual machine.

To do so, it is required to install a recent version of Virtualbox (`> 6.0`):
[Virtual Box][1]

Then, you should download the Nomadic Labs virtual machine using:

    wget https://cloudflare-ipfs.com/ipfs/QmSzsJwRMgQzYnkaugyMvUZfALoF2LHHbs5QnMqkJsfHKN \
        -O nomadic-training_debian-10-amd64.ova

or directly thought IPFS (InterPlanetary File System):

    ipfs get QmSzsJwRMgQzYnkaugyMvUZfALoF2LHHbs5QnMqkJsfHKN && mv QmSzsJwRMgQzYnkaugyMvUZfALoF2LHHbs5QnMqkJsfHKN nomadic-training_debian-10-amd64.ova

- Install IPFS on your machine [IPFS distributions][10] or as a [web browser extension][11]
- Initialize IPFS: `ipfs init`
- Start the IPFS daemon: `ipfs daemon`
- Download the Virtual machine: `ipfs get QmSzsJwRMgQzYnkaugyMvUZfALoF2LHHbs5QnMqkJsfHKN && mv QmSzsJwRMgQzYnkaugyMvUZfALoF2LHHbs5QnMqkJsfHKN nomadic-training_debian-10-amd64.ova`

or direct download : [nomadic-training_debian-10-amd64.ova][2]

Then, you should use the `import appliance` feature in the
virtualbox menu. Make sure that the virtual machine is working by running it **at
least once** (you may need to configure your host to activate such virtualization
software).

## Install dependencies on your own machine

In this section, we give all the dependencies which are necessary to run the
training's development environment. *However, we recommend to download and
setup the virtual machine, just in case …*

### Development tools
  
- Emacs
- [Opam][3] (OCaml Package Manager)
- [LIGO][4] (smart-contract language)
- [Smartpy][5] (smart-contract language)

### Tezos

- [How to get][6]
- [How to use][7]
- [How to run][8]
- Michelson mode for emacs [Michelson mode][9]

After running successfully through all these steps, you should meet the
following requirements:

- Run a Tezos (test network/sandboxed) node
- Use the Michelson emacs mode
- Compile a LIGO piece of code
- Compile a SmartPy piece of code

[1]: https://www.virtualbox.org/
[2]: https://cloud.tzalpha.net/s/pQwkYsQ6gWgMj85
[3]: https://opam.ocaml.org/doc/Install.html 
[4]: https://ligolang.org/docs/intro/installation/
[5]: https://medium.com/@SmartPy_io/introducing-smartpybasic-a-simple-cli-to-build-tezos-smart-contract-in-python-f5bd8772b74a
[6]: http://tezos.gitlab.io/introduction/howtoget.html 
[7]: http://tezos.gitlab.io/introduction/howtouse.html 
[8]: http://tezos.gitlab.io/introduction/howtorun.html
[9]: http://tezos.gitlab.io/user/various.html#environment-for-writing-michelson-contracts
[10]: https://dist.ipfs.io/#go-ipfs
[11]: https://docs.ipfs.io/introduction/usage/#companion-browser-extension

[12]: ../download/tezos_overview.pdf
[13]: ../download/tezos_standards.pdf
[14]: ../download/self-amending_blockchain.pdf
[15]: ../download/future_of_tezos.pdf
[16]: ../download/interact_with_the_blockchain.pdf
[17]: ../download/michelson_smart_contracts.pdf
[18]: https://ligolang.org/docs/tutorials/get-started/tezos-taco-shop-smart-contract/
[19]: https://smartpy.io/docs/Paris.20191024.Nomadic/
[20]: https://gitlab.com/bidinger/try-taquito/
[21]: https://gitlab.com/nomadic-labs/tezos-dapp-demo/tree/test-taquito

<hr/>
