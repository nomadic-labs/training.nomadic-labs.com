Title: Training resources
Save_as: training.html
Template: page

# February 2020 Training

[Past trainings][0]

<hr/>

## Registration (<span style="color:red">closed</span>)
The registration for the February 2020 training session is **now closed!**

If you are interested in participating in the next session, please contact <training@nomadic-labs.com> and follow [@LabosNomades](https://twitter.com/LabosNomades).

## Location

<a href="https://www.openstreetmap.org/#map=19/48.86936/2.33740">
Cloud Business Center *(salle New York)*<br />
10 bis Rue du 4 septembre,<br />
75002 Paris</a>

## Schedule
Training sessions will start 9.30 am, a welcome coffee will be available at 9 am.
The training is divided in four sessions of 1.5h each, two in the morning, two in the
afternoon, with coffee and networking breaks.

Time      | February 11th                 | February 12th           | February 18th                                    | February 19th |
----      | ----                          | ------                  | -------                                          | --------      |
1 slot    | [Introduction][27]            | [Michelson][25]         | [Indexers][30]                                   | HACKING DAY   |
Break     |                               |                         |                                                  |               |
2 slot    | [The "economic protocol"][24] | [LIGO Theory][26]       | [Monitoring][35]                                 | HACKING DAY   |
Lunch     |                               |                         |                                                  |               |
3rd slot  | [Gas/Fee/Burn][29]            | [Tezos CLI][23]         | [Taquito][31]                                    | HACKING DAY   |
Break     |                               |                         |                                                  |               |
4th slot  | [Privacy/ZK][28]              | [Deploy your infra][22] | DAPPS ([slides][32], [subject][33], [repo][34])  | HACKING DAY   |


<hr/>

## Details:

### An introduction to Tezos

A general presentation of the Tezos blockchain, including its history,
its components, and an overview of the current economic protocol.

### The "economic protocol"

A more in-depth presentation of the so-called "economic protocol", the
part of Tezos that can be changed through an amendment procedure.
This will include:

- Proof of Stake (PoS) versus Proof of Work (PoW)
- Liquid PoS and delegation
- Emmy+: Tezos consensus algorithm
- On-chain governance: the amendment procedure
- Anatomy of an operation and different types of operations
- Anatomy of a block and how operations and blocks are encoded
- Smart contracts overview and gas computation

### ZeroKnowledge Proofs in Tezos

### Deploy your infra

- Using docker to easily deploy a node from scratch
- Running a backer on zeronet/babylonnet
- Secure your keys with the `tezos-signer`
- Using `tezos-signer` and a ledger
- Creating a secure production ready infrastructure
  + front-ends nodes
  + private mode
  + hsm ?

### Michelson

- Introduction to Michelson
- Explain how to read a Michelson contract
- Simulate a Michelson contract with [Try Michelson](https://try-michelson.tzalpha.net/)
- A simple example on formally verifying Michelson contracts

### LIGO

- Introduction to Pascal LIGO language (theory)
- Explain how to read a LIGO contract
- Provide a simple annotated LIGO contract for people to deploy
- Change the LIGO contract to add a few feature
- Deploy the LIGO contract using the `tezos-client`

### Block explorers

- What is a block explorer, how it is made and potential pitfalls
- What is an indexer
- How to read your data using a block explorer
- Explore existing block explorers
- What to look for and how to use them

### Monitoring

- How do we monitor the Tezos network?
- Different types of monitors
  + Backer-oriented
  + Development-oriented
- Which RPCs to call
- How to create a basic monitor with [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/)

### Taquito

- Introduction to Taquito: basic module structure
- Setup a simple reason project with Taquito
- Interact with a previously deployed LIGO smart contract
- Reason Taquito bindings
- Setup the inMemory signer
- Setup the [tezBridge](https://docs.tezbridge.com/) signer (with a ledger??)
- Get the balance of a smart contract
- Execute a transaction

### DAPPS

- Build on the Taquito presentation
- Add React to the mix: setup a Reason/React project using Taquito
- Going thought the structure of a React component
- Add bells and whistles
- CSS and final touches
- Deploy the application with Docker on babylonnet or zeronet

# Prerequisites

- A bare metal or a virtual machine running *nix
  + Tezos is well tested on Debian based distributions (Buster)
  + Tezos is easy to install on Darwin based OSes
+ Docker
+ Python3

We provide a [VirtualBox Pre-configured VM](#VirtualBox).

## Install dependencies on your own machine

In this section, we give all the dependencies which are necessary to run the
training's development environment. *However, we recommend to download and
setup the virtual machine, just in case…*

### Development tools

- Your favorite editor
- [LIGO][4] (compiler for the smart-contract language)
- [Try Michelson](https://try-michelson.tzalpha.net/) (Michelson online editor and simulator)

<a name="VirtualBox" />

### VirtualBox

- Install a recent version of [VirtualBox (`> 6.0`)][1]
- Download the Nomadic Labs virtual machine using:

        wget https://cloudflare-ipfs.com/ipfs/QmXN4ryYv7M7bbSUKqQnus7xA2dcWjPNzZCnHgfN4DSqsS \
            -O nomadic-training_debian-10-amd64.ova


    or directly through IPFS (InterPlanetary File System):

        ipfs get QmXN4ryYv7M7bbSUKqQnus7xA2dcWjPNzZCnHgfN4DSqsS \
            && mv QmXN4ryYv7M7bbSUKqQnus7xA2dcWjPNzZCnHgfN4DSqsS nomadic-training_debian-10-amd64.ova


- Install IPFS on your machine [IPFS distributions][10] or as a [web browser extension][11]
- Initialize IPFS: `ipfs init`
- Start the IPFS daemon: `ipfs daemon`
- Download the Virtual machine:

        ipfs get QmXN4ryYv7M7bbSUKqQnus7xA2dcWjPNzZCnHgfN4DSqsS \
            && mv QmXN4ryYv7M7bbSUKqQnus7xA2dcWjPNzZCnHgfN4DSqsS nomadic-training_debian-10-amd64.ova

- Use the `import appliance` feature in the VirtualBox menu. Make sure that the virtual
  machine is working by running it **at least once** (you may need to configure your host
  to activate such virtualization software).

[0]: ./past_trainings.html
[1]: https://www.virtualbox.org/
[4]: https://ligolang.org/docs/intro/installation/
[10]: https://dist.ipfs.io/#go-ipfs
[11]: https://docs.ipfs.io/introduction/usage/#companion-browser-extension

[12]: ../download/tezos_overview.pdf
[13]: ../download/tezos_standards.pdf
[14]: ../download/self-amending_blockchain.pdf
[15]: ../download/future_of_tezos.pdf
[16]: ../download/interact_with_the_blockchain.pdf
[17]: ../download/michelson_smart_contracts.pdf
[18]: https://ligolang.org/docs/tutorials/get-started/tezos-taco-shop-smart-contract/
[19]: https://smartpy.io/docs/Paris.20191024.Nomadic/
[20]: https://gitlab.com/bidinger/try-taquito/
[21]: https://gitlab.com/nomadic-labs/tezos-dapp-demo/tree/test-taquito
[22]: ../download/deploy.pdf
[23]: ../download/cli.pdf
[24]: ../download/economic-protocol.pdf
[25]: ../download/2020-02-12-michelson_smart_contracts.pdf
[26]: ../download/ligo.pdf
[27]: ../download/intro.pdf
[28]: ../download/privacy.pdf
[29]: ../download/gas-and-fees.pdf
[30]: ../download/indexers.pdf
[31]: ../download/taquito.pdf

[32]:https://docs.google.com/presentation/d/12eyJQ8bFQk60xffbBV0sXieSo7Xy2V6CQ66jTCHMVb4/
[33]:https://hackmd.io/@kinokasai/Sy1v-vtmL
[34]:https://gitlab.com/kinokasai/tezos-dapp-practicum/-/tree/solution
[35]: ../download/TezosGigaNode.pdf
<hr/>

<script>
var tables, i;
tables = document.getElementsByTagName('table');
for (i=0;i<tables.length;i++) {
  tables[i].className = 'table is-bordered is-fullwidth';
}
</script>
