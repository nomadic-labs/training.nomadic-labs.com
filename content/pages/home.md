Title:
URL:
Save_as: index.html
Template: home

<div class="columns">
<div class="column">

<h2>About Nomadic Labs</h2>

Nomadic Labs houses a team focused on Research and Development. Our core
competencies are in programming language theory and practice, distributed
systems, and formal verification.  We believe our strength lies in a unique mix
of skills and experience, allowing us to transfer the best of academic research
into real world applications.

</div>

<div class="column">

<h2>Training and Education</h2>

We provide training on a number of topics related to the Tezos World.

<ul>
  <li>Core shell development</li>
  <li>Deployment of Tezos nodes / bakers</li>
  <li>Michelson language training and smart contract verification</li>
  <li>Development of Distributed Applications (DAPPs)</li>
  <li>Protocol Development</li>
</ul>

</div>

</div>

Nomadic Labs is committed to advancing knowledge and improving the state of the
art by providing education on blockchain technology, smart contracts and DAPPS
development, and professional continuing education programs. The Nomadic Labs
Training Portal offers support material for our training courses.

If you have a question related to Tezos, need onsite training, or want to
discuss your learning needs, please feel free to contact us.


