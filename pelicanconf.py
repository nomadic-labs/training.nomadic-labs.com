#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Nomadic Labs'
SITENAME = 'Training @ Nomadic Labs'
SITEURL = 'http://localhost:8000'
SITESUBTITLE = ""
DESCRIPTION = "Nomadic Labs Continuous Training Website"
WEBSITEURL = "https://training.nomadic-labs.com"

CC_LICENSE = "CC-BY-NC"
CC_ATTR_MARKUP = True
CUSTOM_LICENSE = "Creative Commons Attribution-NonCommercial 4.0 International"

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

ARTICLE_TRANSLATION_ID = None
PAGE_TRANSLATION_ID = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

import bulrush, os

THEME_NAME = "bulrush"
THEME = bulrush.PATH
JINJA_ENVIRONMENT = bulrush.ENVIRONMENT
JINJA_FILTERS = bulrush.FILTERS

THEME_TEMPLATES_OVERRIDES = [ "themes/nomadic/templates" ]

PLUGIN_PATHS = ['plugins']
PLUGINS = ['assets']

# Static files
STATIC_PATHS = [
    'download',
    'images',
    'extra',
    'js'
]
EXTRA_PATH_METADATA = {
    'extra/custom.css': {'path': 'theme/css/custom.css'},
    'js/nomadic.js': {'path': 'theme/js/nomadic.js'},
}
