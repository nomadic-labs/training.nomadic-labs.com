
# Pelican based one page static website based on Bulrush

## Setup

We require a version of python >= 3.7

The system dependencies are

- `python3`
- `python3-dev`
- `python3-pip`
- `python3-venv`
- `build-essential`
- `libssl-dev`
- `libffi-dev` 

(debian packages)

You can also use pyenv to manage your python environment.
`curl https://pyenv.run | bash`

To activate the development environment run the command in bash

    export PATH=~/.local/bin:$PATH
    poetry install

## Compilation

    make html

## Structure

The template is based on bulrush installed as a pip package. We override
two templates, `base.html` and `home.html` . The first one is the basic
template for all pages. The second one is used to build the website frontpage


The directory `content` has the following structure

    content\
    content\extra\custom.css  <- override css file
    content\download          <- static download files
    content\js\nomadic.js     <- js files
    content\pages\home.md     <- text of the front page.

The file `content\pages\home.md` is compiled using the `home.html` template.

## Deployment

The gitlab CI reads the .gitlab-ci.html and publish the website on a private
docker registry.

The Dockerfile compiles and create a fresh docker image.

Other files in the directory as `tasks.py` can be ignored.

## Main configuration file

`pelicanconf.py` and `publishconf.py` contain all configuration variables
used by pelican to generate the static website. The first one is use in 
development, while the second contains variables used only for deployment.
